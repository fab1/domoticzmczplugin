#           MCZ Plugin
#
#           Author:     Dnpwwo, 2016 - 2017
#
"""
<plugin key="MCZ" name="MCZ pellet stove controler" author="Fab" version="1.0.0" wikilink="http://www.domoticz.com/wiki/plugins/plugin.html" externallink="https://www.google.com/">
    <description>
		<h2>Module de gestion du poel MCZ</h2>
	</description>
	<params>
		<param field="Address" label="Domoticz IP Address" width="200px" required="true" default="localhost"/>
		<param field="Port" label="Port" width="40px" required="true" default="8080"/>
		<param field="Username" label="Username" width="200px" required="false" default=""/>
		<param field="Password" label="Password" width="200px" required="false" default=""/>
		<param field="Mode1" label="SendCde" width="75px">
            <options>
                <option label="False" value="Simu"/>
                <option label="True" value="Normal"  default="False" />
            </options>
        </param>
		<param field="Mode2" label="thermosat_Id" width="75px" required="true" default=""/>
		
		<param field="Mode3" label="thermometre lvl0" width="75px" required="true" default=""/>
		<param field="Mode4" label="thermometre lvl1" width="75px" required="true" default=""/>
       
        <param field="Mode6" label="Debug" width="75px">
            <options>
                <option label="True" value="Debug"/>
                <option label="False" value="Normal"  default="true" />
            </options>
        </param>
		
		
    </params>
</plugin>
"""
import Domoticz
import sys
import json
import os
import subprocess
import statistics 
import time
import urllib.request as request
import urllib.parse as parse
from datetime import date, time, datetime
Reset_TimeDay = time(0,30, 0)


polingRate= 60 #rate in secondes (must be multiple 10)
n=0
nbr_stk=10
Kp= 60
Ki=1
Kd = 10
Val_Heartbeat=10

class BasePlugin:
    mode = 0
    #0: OFF / 1: Manuel / 2: Auto / 3: Eco
    fan1 = 1
    #1 a 6 -- 7 
    fan2 = 1
    #1 a 6
    puissance = 1
    #1 à 5
    id = 5656834
    Consigne = 20
    temperature_bas = 20
    temperature_haut = 20

    TmpErreur = 0
    DeltaErreur = 0
    SomErreur = 0
    derive_table= []
    Cmd = 0
    nb_demarage = 0
    Demarage_encours = False
    Time = 0

    def onStart(self):
    	if Parameters["Mode6"] == "Debug":
    		Domoticz.Debugging(1)
    		DumpConfigToLog()
    	else:
    		Domoticz.Debugging(0)
    	Domoticz.Debug("onStart called")

    	Options1 = {"LevelActions": "||", 
    				"LevelNames": "Off|Manuel|Auto",
    				"LevelOffHidden": "false",
    				"SelectorStyle": "0"}
    	Options2 = {"LevelActions": "||||||",  
                   "LevelNames": "Off|Lvl_1|Lvl_2|Lvl_3|Lvl_4|Lvl_5|AUto",
                   "LevelOffHidden": "false",
                   "SelectorStyle": "0"}
    	Options3 = {"LevelActions": "||||||", 
                    "LevelNames": "Off|Lvl_1|Lvl_2|Lvl_3|Lvl_4|Lvl_5|AUto",
                    "LevelOffHidden": "false",
                    "SelectorStyle": "1"}
    	Options4 = {"LevelActions": "||||||", 
                    "LevelNames": "Off|Lvl_1|Lvl_2|Lvl_3|Lvl_4|Lvl_5|AUto",
                    "LevelOffHidden": "false",
                    "SelectorStyle": "1"}
					
		
    	self.RefreshTempValues()
    	n=0
    	Domoticz.Debug("Device count: " + str(len(Devices)))
    	Domoticz.Heartbeat(Val_Heartbeat)


    	if(len(Devices) == 0):		
    		Domoticz.Device(Name="Poel",  				Unit=1, Used=1, TypeName="Selector Switch", Switchtype=18, Image=9, Options=Options1).Create()		
    		Domoticz.Device(Name="PuissancePoel",  		Unit=2, Used=1, TypeName="Selector Switch", Switchtype=18, Image=10, Options=Options2).Create()
    		Domoticz.Device(Name="Ventilateur1_Poel", 	Unit=3, Used=1, TypeName="Selector Switch", Switchtype=18, Image=7, Options=Options3).Create()
    		Domoticz.Device(Name="Ventilateur2_Poel",  	Unit=4, Used=1, TypeName="Selector Switch", Switchtype=18, Image=7, Options=Options4).Create()			
    		Domoticz.Log("Devices created.")
    	else:
    		Devices[1].Update(nValue=Devices[1].nValue, sValue=Devices[1].sValue, Options=Options1)
    		#self.mode=Devices[1].nValue/10 Ne correspond pas forcement a l'etat du poel, il faut faire un on/off
    		Devices[2].Update(nValue=Devices[2].nValue, sValue=Devices[2].sValue, Options=Options2)
    		self.puissance=Devices[2].nValue/10
    		Devices[3].Update(nValue=Devices[3].nValue, sValue=Devices[3].sValue, Options=Options3)
    		self.fan1=Devices[3].nValue/10
    		Devices[4].Update(nValue=Devices[4].nValue, sValue=Devices[4].sValue, Options=Options4)
    		self.fan2=Devices[4].nValue/10

    		Domoticz.Log("Devices updated.")  
    	return True

    def onConnect(self, Connection, Status, Description):
        
        return True

    def onMessage(self, Connection, Data):
        Domoticz.Debug("onMessage called")

    def onCommand(self, Unit, Command, Level, Hue):        
	#UNIT 1 : Poel Off|Manuel|Auto", // UNIT2 PuissancePoel Off|Lvl_1|Lvl_2|Lvl_3|Lvl_4|Lvl_5|AUto //UNIT 3 Ventilateur1_Poel /UNIT4  Ventilateur2_Poel
	
        Domoticz.Debug("onCommand called for Unit " + str(Unit) + ": Parameter '" + str(Command) + "', Level: " + str(Level))
		
        if (Unit == 1):#STOVE MODE change
		#in change mode to off send off / if mode manu send last values. / if mode auto send auto to power & fan
            if (Level == 0):  #SWITCH OFF STOVE
                self.mode=0
            elif (Level == 1):  #SWITH ON MANU
                UpdateDevice(2,self.puissance,str(self.puissance))
                UpdateDevice(3,self.puissance,str(self.puissance))
                UpdateDevice(4,self.puissance,str(self.puissance))
                self.mode = 1
            else :
                self.mode=2   # SWITCH ON AUTO Set all variable to auto
                UpdateDevice(2,60,"60")
                UpdateDevice(3,60,"60")
                UpdateDevice(4,60,"60")
		# if power not auto  reset last values / if power auto set mode to auto and fan to auto
        elif (Unit == 2):#STOVE POWER Change
            self.puissance=Level//10
            if (Level !=60): #STOVE POWER NOT AUTO
                UpdateDevice(1,10,"10")
                UpdateDevice(3,self.puissance,str(self.puissance))
                UpdateDevice(4,self.puissance,str(self.puissance))
            else :			#stove power auto 
                UpdateDevice(1,20,"20")
                UpdateDevice(3,60,"60")
                UpdateDevice(4,60,"60")
        elif (Unit == 3):            
            self.fan1=Level//10
        else :
            self.fan2=Level//10

        UpdateDevice(Unit, Level, str(Level))          
        self.SendMCZStove()

    def onNotification(self, Name, Subject, Text, Status, Priority, Sound, ImageFile):
        Domoticz.Debug("Notification: " + Name + "," + Subject + "," + Text + "," + Status + "," + str(Priority) + "," + Sound + "," + ImageFile)

    def onHeartbeat(self):
        global n
        
        #Domoticz.Log("{}".format(datetime.time(datetime.now())))
        #Domoticz.Log("{}".format(Reset_TimeDay))
        #Domoticz.Log("{}".format(Reset_TimeDay+time(0,0,19)))
        duration = (datetime.combine(date.min, datetime.time(datetime.now())) - datetime.combine(date.min, Reset_TimeDay)).total_seconds()


        if duration >0 and duration<Val_Heartbeat :
            Domoticz.Status("Restart Day values. COnfiguration at {}".format(Reset_TimeDay))
            Domoticz.nb_demarage=0
        #Domoticz.Log(str(duration))
        if (n==(polingRate/10 - 1)) :	#heartbet 10 secondes -> tout les 6 coup soit chaque minutes
            n=0
            self.calcul_coef()
            if (int(Devices[1].nValue)/10 != 1): #Calcul STOVE Coefficient only in auto mode
                self.calcul_Poel()
        else :
            n=n+1

        return True

    def onDisconnect(self, Connection):
        Domoticz.Debug("Device has disconnected")
        return

    def onStop(self):
        Domoticz.Debug("onStop called")
        return True
		
    def SendMCZStove(self):
        if Parameters["Mode1"] == "Normal":
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])
            time.sleep(2)
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])
            status = subprocess.check_output(["sudo", "./APPLI_MCZ/src/Appli_Cmd_Arg", str(self.id),str(self.mode), "3", str(self.puissance), str(self.fan1), str(self.fan2)])        
            if Parameters["Mode6"] == "Debug":
                Domoticz.Log(str(status))
        return

    def RefreshTempValues(self):
        APIjson = DomoticzAPI("type=devices&rid={}".format(Parameters["Mode2"]))
        try:
        	nodes = APIjson["result"]
        except:
        	nodes = []
        	Domoticz.Error("no valid result for node {}".format(node["Name"]))
        for node in nodes: 
        	Domoticz.Debug("Consigne with node {} value {}°C".format(node["Name"],node["Data"]))
        self.Consigne=int(float(node["Data"]))
    
        APIjson = DomoticzAPI("type=devices&rid={}".format(Parameters["Mode3"]))
        try:
        	nodes = APIjson["result"]
        except:
        	nodes = []
        	Domoticz.Error("no valid result for node {}".format(node["Name"]))
        for node in nodes: 
        	Domoticz.Debug("Measure value first floor {}°C with node {}".format(node["Temp"],node["Name"]))
        self.temperature_bas=node["Temp"]
    
        APIjson = DomoticzAPI("type=devices&rid={}".format(Parameters["Mode4"]))
        try:
        	nodes = APIjson["result"]
        except:
        	nodes = []
        	Domoticz.Error("no valid result for node {}".format(node["Name"]))
        for node in nodes: 
        	Domoticz.Debug("Measure value first floor {}°C with node {}".format(node["Temp"],node["Name"]))
        self.temperature_haut=node["Temp"]
	
        return

    def calcul_coef(self):
    	global nbr_stk
    	global Ki
    	CmdImax=10
    	self.RefreshTempValues()
    	MoyErr1=0
    	MoyErr2=0
    	#---------------------------------
    	#---  Calcul du proportionnel  ---
    	#---------------------------------

    	#-- Update de la sonde d'erreur
	
    	self.TmpErreur=self.Consigne-self.temperature_bas
    	Domoticz.Debug("calcul {} - {} = {}".format(self.Consigne,self.temperature_bas,self.TmpErreur))

  
    	#------------------------------
    	#---  Calcul de la dérivée  ---
    	#------------------------------
		
    	count = len(self.derive_table)
    	Domoticz.Debug("valeur table {}".format(count))
    	if count < nbr_stk :
    		self.derive_table.insert(count,self.TmpErreur)
    	else : 
    		del self.derive_table[0]
    		self.derive_table.insert(count,self.TmpErreur)
    	#--Le delta erreur est calculé sur la moyenne de 22 erreurs ce qui permet de 'lisser' la variation
    		MoyErr1=statistics.mean(self.derive_table[0:int(count/2)])
    		MoyErr2=statistics.mean(self.derive_table[int(count/2):count])
			
    	self.DeltaErreur=MoyErr2-MoyErr1
    		
 
    	#-------------------------------
    	#---  Calcul de l'intégrale  ---
    	#-------------------------------
    	#-- Calcul de la somme des erreurs + Update de la sonde Somme d'erreur
    	#-- Si la l'erreur est supérieure à +-1° on ne somme pas pour limiter l'impact des changements de consigne
    	if ((self.TmpErreur > -1) and (self.TmpErreur < 1) and (Ki != 0)) : 
    		self.SomErreur = (self.TmpErreur + self.SomErreur)
    		if (self.SomErreur > (CmdImax / Ki)) :
    			self.SomErreur = CmdImax / Ki
    		elif (self.SomErreur < 0) :
    			self.SomErreur = 0

    	#-------------------------------
    	#---  Calcul du coefficeint de puissance désiré  ---
    	#-------------------------------
    	self.Cmd = Kp*self.TmpErreur + Ki*self.SomErreur + Kd*self.DeltaErreur
		#-- La commande de chauffage est un nombre entier compris entre 0 et 100
    	if self.Cmd < 0 : 
    		self.Cmd = 0
    	elif self.Cmd > 100 :
    		self.Cmd = 100
    	else :
    		self.Cmd = round(self.Cmd)
			
    	Domoticz.Debug("chauffage {} erreur {} deltaerreur {} somerreur{}".format(self.Cmd, self.TmpErreur,self.DeltaErreur,self.SomErreur))
		
    	return 

    	return

    def calcul_Poel(self):
    	Domoticz.Log("Calculate STOVE  values : CalculateNededPower : {} - STOVE Mode {} - StartCounter {}".format(self.Cmd,self.mode,self.nb_demarage))
    	if (self.Cmd > 0 and self.mode == 0): # demarage poel si calcul de cmde >0 et mode eteint
    		self.mode = 2
    		self.fan1=6
    		self.fan2=6
    		self.puissance=6
    		self.SendMCZStove()
    		self.demarage = True
    		self.Time = datetime.now()
    		self.nb_demarage=self.nb_demarage+1
    		Domoticz.Status("Demarage poel")
    	elif(self.Cmd <=0 and self.mode == 2) : # on souhaite eteindre le poel
    		if (self.nb_demarage > 4 ) : #on a deja eteint/alumé trop de fois. On laisse allumé
    			Domoticz.Debug("Stove can't switch off. Max start reach")
    			self.fan1=1
    			self.fan2=1
    			self.puissance=1
    			self.SendMCZStove()
    		elif ((datetime.now()-self.Time).total_seconds()<(30*60)) : # Temps de demarage trop court. On reduit mais on ne coupe pas
    			Domoticz.Debug("Stove can't switch off. Wait more {} secondes".format((datetime.now()-self.Time).total_seconds()-(30*60)))
    			self.fan1=1
    			self.fan2=1
    			self.puissance=1
    			self.SendMCZStove()
    		else :				# si aucun cas particulier alors on coupe 
    			self.mode = 0
    			Domoticz.Status("Coupure poel")
				
    		self.SendMCZStove()
    	else: 
    		if (self.Cmd>0 and self.Cmd<20):
    			self.puissance=1
    		elif (self.Cmd>=20 and self.Cmd<40):
    			self.puissance=2
    		elif (self.Cmd>=40 and self.Cmd<60):
    			self.puissance=3
    		elif (self.Cmd>=60 and self.Cmd<80):
    			self.puissance=4
    		elif (self.Cmd>=80):
    			self.puissance=5
		
    	return
		



global _plugin
_plugin = BasePlugin()

def onStart():
    global _plugin
    _plugin.onStart()

def onStop():
    global _plugin
    _plugin.onStop()

def onConnect(Connection, Status, Description):
    global _plugin
    _plugin.onConnect(Connection, Status, Description)

def onMessage(Connection, Data):
    global _plugin
    _plugin.onMessage(Connection, Data)

def onCommand(Unit, Command, Level, Hue):
    global _plugin
    _plugin.onCommand(Unit, Command, Level, Hue)

def onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile):
    global _plugin
    _plugin.onNotification(Name, Subject, Text, Status, Priority, Sound, ImageFile)

def onDisconnect(Connection):
    global _plugin
    _plugin.onDisconnect(Connection)

def onHeartbeat():
    global _plugin
    _plugin.onHeartbeat()

# Generic helper functions
def DumpConfigToLog():
    for x in Parameters:
        if Parameters[x] != "":
            Domoticz.Debug( "'" + x + "':'" + str(Parameters[x]) + "'")
    Domoticz.Debug("Settings count: " + str(len(Settings)))
    for x in Settings:
        Domoticz.Debug( "'" + x + "':'" + str(Settings[x]) + "'")
    Domoticz.Debug("Image count: " + str(len(Images)))
    for x in Images:
        Domoticz.Debug( "'" + x + "':'" + str(Images[x]) + "'")
    Domoticz.Debug("Device count: " + str(len(Devices)))
    for x in Devices:
        Domoticz.Debug("Device:           " + str(x) + " - " + str(Devices[x]))
        Domoticz.Debug("Device ID:       '" + str(Devices[x].ID) + "'")
        Domoticz.Debug("Device Name:     '" + Devices[x].Name + "'")
        Domoticz.Debug("Device nValue:    " + str(Devices[x].nValue))
        Domoticz.Debug("Device sValue:   '" + Devices[x].sValue + "'")
        Domoticz.Debug("Device LastLevel: " + str(Devices[x].LastLevel))
        Domoticz.Debug("Device Image:     " + str(Devices[x].Image))
    return
 
def UpdateDevice(Unit, nValue, sValue):
    # Make sure that the Domoticz device still exists (they can be deleted) before updating it 
    if (Unit in Devices):
        if (Devices[Unit].nValue != nValue) or (Devices[Unit].sValue != sValue):
            Devices[Unit].Update(nValue=nValue, sValue=str(sValue))
            Domoticz.Log("Update "+str(nValue)+":'"+str(sValue)+"' ("+Devices[Unit].Name+")")
    return


def DomoticzAPI(APICall):
    resultJson = None
    url = "http://{}:{}/json.htm?{}".format(Parameters["Address"], Parameters["Port"], parse.quote(APICall, safe="&="))
    Domoticz.Debug("Calling domoticz API: {}".format(url))
    try:
        req = request.Request(url)
        if Parameters["Username"] != "":
            Domoticz.Debug("Add authentification for user {}".format(Parameters["Username"]))
            credentials = ('%s:%s' % (Parameters["Username"], Parameters["Password"]))
            encoded_credentials = base64.b64encode(credentials.encode('ascii'))
            req.add_header('Authorization', 'Basic %s' % encoded_credentials.decode("ascii"))

        response = request.urlopen(req)
        if response.status == 200:
            resultJson = json.loads(response.read().decode('utf-8'))
            if resultJson["status"] != "OK":
                Domoticz.Error("Domoticz API returned an error: status = {}".format(resultJson["status"]))
                resultJson = None
        else:
            Domoticz.Error("Domoticz API: http error = {}".format(response.status))
    except:
        Domoticz.Error("Error calling '{}'".format(url))
    return resultJson



