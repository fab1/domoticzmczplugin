#!/usr/bin/env bash
set -e


######## VARIABLES #########
setupVars=/etc/domoticz/setupVars.conf

useUpdateVars=false

Dest_folder=""
Temp_folder="/tmp/DomoticzMCZPlugin"



# Compatibility

if [ -x "$(command -v apt-get)" ]; then
    #Debian Family
    #############################################
    PKG_MANAGER="apt-get"
    PKG_CACHE="/var/lib/apt/lists/"
    UPDATE_PKG_CACHE="${PKG_MANAGER} update"
    PKG_UPDATE="${PKG_MANAGER} upgrade"
    PKG_INSTALL="${PKG_MANAGER} --yes --fix-missing install"
    # grep -c will return 1 retVal on 0 matches, block this throwing the set -e with an OR TRUE
    PKG_COUNT="${PKG_MANAGER} -s -o Debug::NoLocking=true upgrade | grep -c ^Inst || true"
    INSTALLER_DEPS=( git wiringpi libmxml-dev python3 python3-pip )
    #PYTHON_DEPS=( python-miio ptvsd rpdb )
    package_check_install() {
        dpkg-query -W -f='${Status}' "${1}" 2>/dev/null | grep -c "ok installed" || 'sudo ' ${PKG_INSTALL} "${1}"
    }
    PIP_INSTALL="pip3 install"
    pip_install_list=$(pip3 list --format=columns)
    pip_check_install() {
        echo $pip_install_list | grep -c "${1}" || ${PIP_INSTALL} "$@"
    }
else
    echo "OS distribution not supported"
    exit
fi

####### FUNCTIONS ##########
spinner() {
    local pid=$1
    local delay=0.50
    local spinstr='/-\|'
    while [ "$(ps a | awk '{print $1}' | grep "${pid}")" ]; do
        local temp=${spinstr#?}
        printf " [%c]  " "${spinstr}"
        local spinstr=${temp}${spinstr%"$temp"}
        sleep ${delay}
        printf "\b\b\b\b\b\b"
    done
    printf "    \b\b\b\b"
}


update_package_cache() {
    #Running apt-get update/upgrade with minimal output can cause some issues with
    #requiring user input (e.g password for phpmyadmin see #218)

    #Check to see if apt-get update has already been run today
    #it needs to have been run at least once on new installs!
    timestamp=$(stat -c %Y ${PKG_CACHE})
    timestampAsDate=$(date -d @"${timestamp}" "+%b %e")
    today=$(date "+%b %e")

    if [ ! "${today}" == "${timestampAsDate}" ]; then
        #update package lists
        echo ":::"
        echo -n "::: ${PKG_MANAGER} update has not been run today. Running now..."
        ${UPDATE_PKG_CACHE} &> /dev/null & spinner $!
        echo " done!"
    fi
}

notify_package_updates_available() {
  # Let user know if they have outdated packages on their system and
  # advise them to run a package update at soonest possible.
    echo ":::"
    echo -n "::: Checking ${PKG_MANAGER} for upgraded packages...."
    updatesToInstall=$(eval "${PKG_COUNT}")
    echo " done!"
    echo ":::"
    if [[ ${updatesToInstall} -eq "0" ]]; then
        echo "::: Your system is up to date! Continuing with Domoticz installation..."
    else
        echo "::: There are ${updatesToInstall} updates available for your system!"
        echo "::: We recommend you run '${PKG_UPDATE}' after installing Domoticz! "
        echo ":::"
    fi
}

install_dependent_packages() {
    # Install packages passed in via argument array
    # No spinner - conflicts with set -e
    declare -a argArray1=("${!1}")

    for i in "${argArray1[@]}"; do
        echo -n ":::    Checking for $i..."
        package_check_install "${i}" &> /dev/null
        echo " installed!"
    done
}

install_packages() {
    # Update package cache
    update_package_cache

    # Notify user of package availability
    notify_package_updates_available

    # Install packages used by this installation script
    install_dependent_packages INSTALLER_DEPS[@]

    # Install packages used by the Domoticz
   # install_python_packages PYTHON_DEPS[@]
}


downloadDomoticzPlugin() {
    if [[ -e $Temp_folder ]]; then
        rm -f -r $Temp_folder
    fi
    echo "::: Creating ${Temp_folder}"
    mkdir $Temp_folder
    cd $Temp_folder
    # Get plugin
    echo "::: Clone gitlab"
    git clone https://gitlab.com/fab1/domoticzmczplugin.git &> /dev/null
    echo $Temp_folder
    cd "./domoticzmczplugin"
}


downloadAPPLIMCZ(){
      
    if [[ -e "${Dest_folder}/plugins/MCZStove/APPLI_MCZ" ]]; then
        rm -f -r "${Dest_folder}/plugins/MCZStove/APPLI_MCZ" 
    fi
    echo "::: Creating APPLI_MCZ"
    echo "folder ${Dest_folder}/plugins/MCZStove/"
    cd "${Dest_folder}/plugins/MCZStove"

    # Get plugin
    echo "::: Clone gitlab"
    git clone https://github.com/KerberRF/APPLI_MCZ.git
    cd "./APPLI_MCZ/src"
    make
}


installDomoticzPlugin() {
    echo "install"
    cd "$Temp_folder/domoticzmczplugin"
    if [[ ! -e "${Dest_folder}/plugins/" ]]; then
        mkdir "${Dest_folder}/plugins/"
        chown "${Current_user}":"${Current_user}" "${Dest_folder}/plugins/"
    fi
    # Move plugin
    echo "::: Destination folder=${Dest_folder}/plugins/"
    for d in */; do
       # if echo "${choices[@]}" | grep -w "$d" &>/dev/null; then
            echo -n ":::     Move plugin ${d}..."
            cp -a $d "${Dest_folder}/plugins/"
            chown -R "${Current_user}":"${Current_user}" "${Dest_folder}/plugins/${d}"
            chmod +x "${Dest_folder}/plugins/${d}plugin.py"
            echo " done!"
       # fi
    done
    
    # Remove temp files
    rm -f -r $Temp_folder
}

find_current_user() {
    # Find current user
    Current_user=${SUDO_USER:-$USER}
    echo "::: Current User: ${Current_user}"
}

stop_service() {
    # Stop service passed in as argument.
    echo ":::"
    echo -n "::: Stopping ${1} service..."
    if [ -x "$(command -v service)" ]; then
        service "${1}" stop &> /dev/null & spinner $! || true
    fi
    echo " done."
}

start_service() {
    # Start/Restart service passed in as argument
    # This should not fail, it's an error if it does
    echo ":::"
    echo -n "::: Starting ${1} service..."
    if [ -x "$(command -v service)" ]; then
        service "${1}" restart &> /dev/null  & spinner $!
    fi
    echo " done."
}

main(){
    
    install_packages
    downloadDomoticzPlugin

    find_current_user

    Dest_folder="/home/${Current_user}/domoticz"

    if [[ -f ${setupVars} ]]; then
        useUpdateVars=false
    else
        echo "Domoticz not installed!"
        exit
    fi

    if [[ ${useUpdateVars} == false ]]; then
        # Display welcome dialogs
    # welcomeDialogs
        # Install and log everything to a file
        #chooseServices
    # chooseDestinationFolder
        installDomoticzPlugin
        downloadAPPLIMCZ
    fi


    echo "::: Restarting services..."
        # ReStart services
        cd ${Dest_folder}
        stop_service domoticz.sh
        start_service domoticz.sh
        echo "::: done."

        echo ":::"
        if [[ "${useUpdateVars}" == false ]]; then
            echo "::: Installation Complete!"
        else
            echo "::: Update complete!"
        fi

}

main "$@"

echo ${Dest_folder}
